// ===========
function kiemTraNhap(giaTri) {
  if (giaTri === 8) {
    alert("Số Tám");
  } else if (giaTri === 9) {
    alert("Số chín");
  } else {
    alert("Không phải 8 và chín");
  }
}
// ===========
function xetHocLucSinhVien(dtb) {
  var thongBao = "";
  switch (dtb) {
    case 9:
      thongBao = "Học lực Xuất sắc";
      break;
    case 9:
      thongBao = "Học lực Giỏi";
      break;
    case 9:
      thongBao = "Học lực Khá";
      break;
    case 9:
      thongBao = "Học lực Trung bình";
      break;
    default:
      thongBao = "Chưa xét được học lực";
  }
  alert(thongBao);
}
// ===========
function xuat10Cau() {
  for (var i = 0; i < 10; i++) {
    document.write("Hello World! lần thứ " + i + "<br>");
  }
}
// ===========
function vongLapWhile(toiDa) {
  var i = 0;
  while (i < toiDa) {
    document.write(`Vòng While ${i}<br>`);
    i++;
  }
}
// ===========
function vongLapDoWhile(toiDa) {
  var i = 0;
  do {
    document.write("Vòng lập DoWhile thứ " + i + "<br>");
    i++;
  } while (i < toiDa);
}
// ===========
function changeColor(choose) {
  choose = +choose;
  var txt = document.getElementById("test");
  if (choose === 1) {
    txt.style.border = "solid 4px red";
  } else {
    txt.style.border = "solid 1px grey";
  }
}
// ===========
function thayDoiChon() {
  var theP = document.getElementById("xuat");
  var dropdown = document.getElementById("chon");
  theP.innerHTML = dropdown.value;
}
// ===========
function changeColorButton(thamSo) {
  +thamSo;
  var nut = document.getElementById("nut");
  if (thamSo === 1) {
    //hover
    console.log("mouseover");
    nut.style.backgroundColor = "red";
  } else if (thamSo === 2) {
    console.log("mouseout");
    nut.style.backgroundColor = "white";
  } else if (thamSo === 3) {
    console.log("mousedown");
    nut.style.backgroundColor = "green";
  } else {
    console.log("mouseup");
    nut.style.backgroundColor = "yellow";
  }
}

var nutBai40 = document.getElementById("nut");
nutBai40.addEventListener("mouseover", function() {
  changeColorButton(1);
});

nutBai40.addEventListener("mouseout", function() {
  changeColorButton(2);
});

nutBai40.addEventListener("mousedown", function() {
  changeColorButton(3);
});

nutBai40.addEventListener("mouseup", function() {
  changeColorButton(4);
});
// ===========
// DOM1
function layGiaTri() {
  var theP = document.getElementById("noiDung");
  theP.innerHTML = " Hello World !";
  theP.style.color = "red";
}
// ===========
// DOM2
function layGiaTriTheP2() {
  var tatCaPTrongDom1 = document.querySelectorAll(".dom1 p");
  alert(
    `Số thẻ P: ${tatCaPTrongDom1.length}\nNội dung thẻ P: ${tatCaPTrongDom1[3].innerHTML} `
  );
}

function layGiaTriClass() {
  var tatCaP = document.getElementsByClassName("demoDom2");
  alert(`Số thẻ P: ${tatCaP.length}\nNội dung thẻ P: ${tatCaP[0].innerHTML}`);
}

// ===========
// DOM3

function createTable() {
  var divId = document.getElementById("container");
  var soDong = +document.getElementById("txtSoDong").value;
  var soCot = +document.getElementById("txtSoCot").value;

  var tagTable = document.createElement("table");
  tagTable.border = 1;

  //   Cách 1
  //   var tableHTML = "";
  //     for (var i = 0; i < soDong; i++) {
  //       tableHTML += `<tr>${createTdInRow(i, soCot)}</tr>`;
  //     }
  //     tagTable.innerHTML = tableHTML;
  //     function createTdInRow(soDong, soCot) {
  //       var html = "";
  //       for (var j = 0; j < soCot; j++) {
  //         html += `<td> R${soDong}, C${j}</td> `;
  //       }
  //       return html;
  //     }
  //====================================
  // Cách 2
  //   for (var i = 0; i < soDong; i++) {
  //     var tagTR = document.createElement("tr");
  //     tagTable.appendChild(tagTR);
  //     for (var j = 0; j < soCot; j++) {
  //       var tagTD = document.createElement("td");
  //       var textNode = document.createTextNode(i + "," + j);
  //       tagTD.appendChild(textNode);
  //       tagTR.appendChild(tagTD);
  //     }
  //   }
  //====================================
  //   Cách 3
  for (var i = 0; i < soDong; i++) {
    var tagTR = document.createElement("tr");
    for (var j = 0; j < soCot; j++) {
      tagTR.innerHTML += `<td> R${i}, C${j} </td>`;
    }
    tagTable.appendChild(tagTR);
  }
  //====================================
  divId.appendChild(tagTable);
}

// ===========
// DOM4
function tinhTong() {
  var soA = document.getElementById("soA").value;
  var soB = document.getElementById("soB").value;
  var kq = document.getElementById("kq");

  if (isNaN(soA) || isNaN(soB)) {
    alert("Chỉ nhập số vào số a và b");
  } else {
    var tong = soA * 1 + soB * 1;
    kq.value = tong;
  }
}
// ===========
//DOM10
function xacNhan() {
  var xacNhan = confirm("Allahu Akbar");
  if (xacNhan) {
    document.write("Thank you");
  } else {
    document.write("You're welcome");
  }
}

function nhapDuLieu() {
  var ten = prompt("Bạn tên gì ?");
  document.write(`Hello ${ten}!`);
}
// ===========
//DOM11
function changeTextColor() {
  document.getElementById("domCss").style.color = "red";
}
function changeBgColor() {
  document.getElementById("domCss").style.background = "yellow";
}
function changeFontSize() {
  document.getElementById("domCss").style.fontSize = "30px";
}
function changeMargin() {
  document.getElementById("domCss").style.marginTop = "130px";
}
function changeImage() {
  var image = document.getElementById("myImage");
  if (image.src.match("bulbon")) {
    image.src = "./img/pic_bulboff.gif";
  } else {
    image.src = "./img/pic_bulbon.gif";
  }
}
